﻿using fap_api_cursos.Data;
using fap_api_cursos.Models;
using System.Linq;

namespace fap_api_cursos.Repositories
{
    public class UsuarioRepository
    {
        private readonly DBContext _context;

        public UsuarioRepository(DBContext context)
        {
            _context = context;
        }
        public Usuario Get(string login, string senha)
        {
            var pessoa = _context.Usuario
                    .Where(x => x.Login.ToLower() == login.ToLower() && x.Senha == senha)
                    .FirstOrDefault();
                                               
            return pessoa;
        }
    }
}
