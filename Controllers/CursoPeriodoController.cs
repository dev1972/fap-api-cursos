﻿using fap_api_cursos.Data;
using fap_api_cursos.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fap_api_cursos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CursoPeriodoController : ControllerBase
    {
        private readonly DBContext _context;

        public CursoPeriodoController(DBContext context)
        {
            _context = context;
        }

        // GET: api/CursoPeriodo
        [HttpGet]
        [Authorize(Roles = "User, DBA, Admin")]
        public async Task<ActionResult<IEnumerable<CursoPeriodo>>> GetCursoPeriodo()
        {
            return await _context.CursoPeriodo
                    .Include(CursoPeriodo => CursoPeriodo.Curso)
                    .Include(CursoPeriodo => CursoPeriodo.Periodo).ToListAsync();
                
        }

        // GET: api/CursoPeriodo/20
        [HttpGet("{IdCurso}")]
        [Authorize(Roles = "User, DBA, Admin")]
        public async Task<ActionResult<IEnumerable<CursoPeriodo>>> GetCursoPeriodo(int IdCurso)
        {
            var cursoPeriodo = await _context.CursoPeriodo
                    .Where(i => i.IdCurso == IdCurso)
                    .Include(CursoPeriodo => CursoPeriodo.Curso)
                    .Include(CursoPeriodo => CursoPeriodo.Periodo)
                    .ToListAsync();

            if (cursoPeriodo == null)
            {
                return NotFound();
            }

            return cursoPeriodo;

        }
    }
}