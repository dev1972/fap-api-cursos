﻿using fap_api_cursos.Data;
using fap_api_cursos.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace fap_api_cursos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DisciplinaController : ControllerBase
    {
        private readonly DBContext _context;

        public DisciplinaController(DBContext context)
        {
            _context = context;
        }

        // GET: api/Disciplinas
        [HttpGet]
        [Authorize(Roles = "User, DBA, Admin")]
        public async Task<ActionResult<IEnumerable<Disciplina>>> GetDisciplina()
        {
            return await _context.Disciplina.ToListAsync();
        }

        // GET: api/Disciplinas/5
        [HttpGet("{id}")]
        [Authorize(Roles = "User, DBA, Admin")]
        public async Task<ActionResult<Disciplina>> GetDisciplina(int id)
        {
            var disciplina = await _context.Disciplina.FindAsync(id);

            if (disciplina == null)
            {
                return NotFound();
            }

            return disciplina;
        }

        [HttpPost]
        [Authorize(Roles = "User, DBA, Admin")]
        public async Task<ActionResult<Disciplina>> PostCurso(Disciplina disciplina)
        {
            _context.Disciplina.Add(disciplina);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDisciplina", new { id = disciplina.Id }, disciplina);
        }
    }
}