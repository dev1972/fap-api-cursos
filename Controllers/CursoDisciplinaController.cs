﻿using fap_api_cursos.Data;
using fap_api_cursos.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fap_api_cursos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CursoDisciplinaController : ControllerBase
    {
        private readonly DBContext _context;

        public CursoDisciplinaController(DBContext context)
        {
            _context = context;
        }

        // GET: api/CursoDisciplina
        [HttpGet]
        [Authorize(Roles = "User, DBA, Admin")]
        public async Task<ActionResult<IEnumerable<CursoDisciplina>>> GetCursoDisciplina()
        {
            return await _context.CursoDisciplina
                    .Include(CursoDisciplina => CursoDisciplina.Curso)
                    .Include(CursoDisciplina => CursoDisciplina.Disciplina).ToListAsync();
                
        }

        // GET: api/Cursos/5
        [HttpGet("{idCurso}")]
        [Authorize(Roles = "User, DBA, Admin")]
        public async Task<ActionResult<IEnumerable<CursoDisciplina>>> GetCurso(int idCurso)
        {
            var cursoDisciplina = await _context.CursoDisciplina
                .Where(i => i.IdCurso == idCurso)
                .Include(CursoDisciplina => CursoDisciplina.Disciplina)
                .Include(CursoDisciplina => CursoDisciplina.Curso)
                .ToListAsync();

            if (cursoDisciplina == null)
            {
                return NotFound();
            }

            return cursoDisciplina;
        }

    }
}