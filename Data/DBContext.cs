﻿using fap_api_cursos.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace fap_api_cursos.Data
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.LogTo(Console.WriteLine);

        }

        public DbSet<Models.Usuario> Usuario { get; set; }

        public DbSet<Models.Aula> Aula { get; set; }

        public DbSet<Curso> Curso { get; set; }

        public DbSet<Models.CursoDisciplina> CursoDisciplina { get; set; }

        public DbSet<Models.CursoPeriodo> CursoPeriodo { get; set; }

        public DbSet<Models.DiasSemana> DiasSemana { get; set; }

        public DbSet<Models.Disciplina> Disciplina { get; set; }

        public DbSet<Models.DisciplinaSemestre> DisciplinaSemestre { get; set; }

        public DbSet<Models.Horario> Horario { get; set; }

        public DbSet<Models.HorarioAula> HorarioAula { get; set; }

        public DbSet<Models.Periodo> Periodo { get; set; }

        public DbSet<Models.Semestre> Semestre { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CursoDisciplina>().HasKey(cd => new { cd.IdCurso, cd.IdDisciplina });

            modelBuilder.Entity<CursoDisciplina>()
                .HasOne<Curso>(cd => cd.Curso)
                .WithMany(a => a.CursoDisciplinas)
                .HasForeignKey(cd => cd.IdCurso);


            modelBuilder.Entity<CursoDisciplina>()
                .HasOne<Disciplina>(cd => cd.Disciplina)
                .WithMany(a => a.CursoDisciplinas)
                .HasForeignKey(cd => cd.IdDisciplina);

            modelBuilder.Entity<CursoPeriodo>().HasKey(cp => new { cp.IdCurso, cp.IdPeriodo });

            modelBuilder.Entity<CursoPeriodo>()
                .HasOne<Curso>(cd => cd.Curso)
                .WithMany(a => a.CursoPeriodos)
                .HasForeignKey(cd => cd.IdCurso);

            modelBuilder.Entity<CursoPeriodo>()
                .HasOne<Periodo>(cd => cd.Periodo)
                .WithMany(a => a.CursoPeriodos)
                .HasForeignKey(cd => cd.IdPeriodo);

            modelBuilder.Entity<DisciplinaSemestre>().HasKey(ds => new { ds.IdDisciplina, ds.IdSemestre });

            modelBuilder.Entity<DisciplinaSemestre>()
                .HasOne<Disciplina>(cd => cd.Disciplina)
                .WithMany(a => a.DisciplinaSemestres)
                .HasForeignKey(cd => cd.IdDisciplina);

            modelBuilder.Entity<DisciplinaSemestre>()
                .HasOne<Semestre>(cd => cd.Semestre)
                .WithMany(a => a.DisciplinaSemestres)
                .HasForeignKey(cd => cd.IdSemestre);

            modelBuilder.Entity<HorarioAula>().HasKey(ha => new { ha.IdHorario, ha.IdAula });

            modelBuilder.Entity<HorarioAula>()
                .HasOne<Horario>(cd => cd.Horario)
                .WithMany(a => a.HorarioAulas)
                .HasForeignKey(cd => cd.IdHorario);

            modelBuilder.Entity<HorarioAula>()
                .HasOne<Aula>(cd => cd.Aula)
                .WithMany(a => a.HorarioAulas)
                .HasForeignKey(cd => cd.IdAula);
        }
    }
}