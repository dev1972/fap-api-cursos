﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace fap_api_cursos.Models
{
    [Table("aula")]
    public class Aula
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nomeaula")]
        public string NomeAula { get; set; }
        [JsonIgnore]
        public IList<HorarioAula> HorarioAulas { get; set; }
    }
}