﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace fap_api_cursos.Models
{
    [Table("curso")]
    public class Curso
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nomecurso")]
        public string NomeCurso { get; set; }
        [JsonIgnore]
        public IList<CursoDisciplina> CursoDisciplinas { get; set; }
        [JsonIgnore]
        public IList<CursoPeriodo> CursoPeriodos { get; set; }

    }
}