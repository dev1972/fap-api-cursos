﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace fap_api_cursos.Models
{
    [Table("periodo")]
    public class Periodo
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nomeperiodo")]
        public string NomePeriodo { get; set; }
        [JsonIgnore]
        public IList<CursoPeriodo> CursoPeriodos { get; set; }
    }
}