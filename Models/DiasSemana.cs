﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace fap_api_cursos.Models
{
    [Table("diassemana")]
    public class DiasSemana
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nomediasemana")]
        public string NomeDiaSemana { get; set; }
    }
}