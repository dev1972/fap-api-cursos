﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace fap_api_cursos.Models
{
    [Table("cursodisciplina")]
    public class CursoDisciplina
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("idcurso")]
        [ForeignKey("Curso")]
        public int IdCurso { get; set; }
        public Curso Curso { get; set; }

        [Column("iddisciplina")]
        [ForeignKey("Disciplina")]
        public int IdDisciplina { get; set; }
        public Disciplina Disciplina { get; set; }

    }
}