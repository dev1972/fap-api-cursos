﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace fap_api_cursos.Models
{
    [Table("disciplina")]
    public class Disciplina
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nomedisciplina")]
        public string NomeDisciplina { get; set; }
        [Column("horasaula")]
        public int HorasAula { get; set; }
        [JsonIgnore]
        public IList<CursoDisciplina> CursoDisciplinas { get; set; }
        [JsonIgnore]
        public IList<DisciplinaSemestre> DisciplinaSemestres { get; set; }
    }
}