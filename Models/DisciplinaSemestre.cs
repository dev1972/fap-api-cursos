﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace fap_api_cursos.Models
{
    [Table("disciplinasemestre")]
    public class DisciplinaSemestre
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("iddisciplina")]
        [ForeignKey("Disciplina")]
        public int IdDisciplina { get; set; }
        public Disciplina Disciplina { get; set; }

        [Column("idsemestre")]
        [ForeignKey("Semestre")]
        public int IdSemestre { get; set; }
        public Semestre Semestre { get; set; }

    }
}