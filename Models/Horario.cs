﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace fap_api_cursos.Models
{
    [Table("horario")]
    public class Horario
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("hora")]
        public TimeSpan Hora { get; set; }
        [JsonIgnore]
        public IList<HorarioAula> HorarioAulas { get; set; }
    }
}