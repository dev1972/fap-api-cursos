﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace fap_api_cursos.Models
{
    [Table("semestre")]
    public class Semestre
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("nomesemestre")]
        public string NomeSemestre { get; set; }
        [JsonIgnore]
        public IList<DisciplinaSemestre> DisciplinaSemestres { get; set; }
    }
}