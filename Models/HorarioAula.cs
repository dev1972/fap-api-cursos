﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace fap_api_cursos.Models
{
    [Table("horarioaula")]
    public class HorarioAula
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("idhorario")]
        [ForeignKey("Horario")]
        public int IdHorario { get; set; }
        public Horario Horario { get; set; }

        [Column("idaula")]
        [ForeignKey("Aula")]
        public int IdAula { get; set; }
        public Aula Aula { get; set; }

    }
}