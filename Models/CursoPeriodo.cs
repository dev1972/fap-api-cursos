﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace fap_api_cursos.Models
{
    [Table("cursoperiodo")]
    public class CursoPeriodo
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("idcurso")]
        [ForeignKey("Curso")]
        public int IdCurso { get; set; }
        public Curso Curso { get; set; }

        [Column("idperiodo")]
        [ForeignKey("Periodo")]
        public int IdPeriodo { get; set; }
        public Periodo Periodo { get; set; }

    }
}